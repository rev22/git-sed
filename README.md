The git-sed script can be used to apply a sed expression to file names and content.

git-sed will refuse running if there are unstaged changes in your worktree.


Usage: git-sed OPTIONS* -- SEDEXPR FILES*

Options:
  --content    - Apply sed expression to content.
  --filenames  - Apply sed expression to filenames.
  --both       - Apply sed expression to both content and filenames

Extra options:
  --greprx    REGEXP  - Select only files matching REGEXP
  --sedopts   ARGS    - Pass ARGS to sed
  --test              - When combined with sed 'p' modifer, print lines or filenames that would change, without applying any actual change. 
  --backupext .EXT    - Backup files before changing them to file.EXT


Copyright (c) 2011, 2013, 2014 Michele Bini <michele.bini@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the version 3 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

